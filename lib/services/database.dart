import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseService {



  final CollectionReference clients =  FirebaseFirestore.instance.collection('clientes');


CollectionReference clientes = FirebaseFirestore.instance.collection('clientes');
CollectionReference platillos = FirebaseFirestore.instance.collection('platillos');
CollectionReference bebidas = FirebaseFirestore.instance.collection('bebidas');
CollectionReference facturas = FirebaseFirestore.instance.collection('facturas');
CollectionReference mesas = FirebaseFirestore.instance.collection('mesas');
CollectionReference meseros = FirebaseFirestore.instance.collection('meseros');


    Future<void> addUser(String nombre, String apellido, String comentario ) {
      // Call the user's CollectionReference to add a new user
      return clientes
          .add({
            'nombre': nombre,
            'apellido': apellido,
            'comentario': comentario
          })
          .then((value) => print("User Added"))
          .catchError((error) => print("Failed to add user: $error"));
    }

    Future<void> addPlatillo(String nombre, String importe ) {
      // Call the user's CollectionReference to add a new user
      return platillos
          .add({
            'nombre': nombre,
            'importe': importe, // Stokes and Sons
          })
          .then((value) => print("Platillo Added"))
          .catchError((error) => print("Failed to add Platillo: $error"));
    }

    Future<void> addBebida(String nombre, String importe ) {
      // Call the user's CollectionReference to add a new user
      return bebidas
          .add({
            'nombre': nombre,
            'importe': importe, // Stokes and Sons
          })
          .then((value) => print("Platillo Added"))
          .catchError((error) => print("Failed to add Platillo: $error"));
    }

     Future<void> addMesero(String nombre, String apellido1, String apellido2 ) {
      // Call the user's CollectionReference to add a new user
      return meseros
          .add({
            'nombre': nombre,
            'apellido1': apellido1,
            'apellido2': apellido2
          })
          .then((value) => print("User Added"))
          .catchError((error) => print("Failed to add user: $error"));
    }

}