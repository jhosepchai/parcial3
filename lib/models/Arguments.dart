import 'package:flutter/material.dart';
class ScreenArguments {
  final Widget form;
  final String type;
  final String action;

  ScreenArguments(this.form, this.type, this.action);
}