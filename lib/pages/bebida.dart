import 'package:flutter/material.dart';
import 'package:parcial03/models/Arguments.dart';
import 'package:parcial03/services/database.dart';

class Bebida extends StatefulWidget {
  Bebida({Key? key}) : super(key: key);

  @override
  _BebidaState createState() => _BebidaState();
}

class _BebidaState extends State<Bebida> {
    @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("BEBIDAS")),
      body: Container(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context,"forms", arguments: ScreenArguments(bebidaForm(), "CLIENTE", "ADD"));         
        },
        child: const Icon(Icons.add_outlined),
      ),
    );
  }

    Widget bebidaForm (){
  var form = Container(
    margin: EdgeInsets.all(10),
    child: Column(
      children :getFormWidget()
    )
  );

  return form;
}

  List<Widget> getFormWidget() {
    final _formKey = GlobalKey<FormState>();
  var _passKey = GlobalKey<FormFieldState>();

  final nameController = TextEditingController();
  final importeController = TextEditingController();
  
 void submit () {
   DatabaseService().addBebida(nameController.text, importeController.text);
  }
    List<Widget> formWidget = [];

    formWidget.add(
      Container(
        margin: EdgeInsets.all(5),
        child: Text("NUEVA BEBIDA", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
      )
      
    );

    formWidget.add(
      new TextField(
          decoration: InputDecoration(
              hintText: 'Nombre',
              labelText: 'Nombre de la bebida'),
              controller: nameController,
    ),
  );
   
    formWidget.add(
      new TextField(
          decoration: InputDecoration(
              hintText: 'Importe',
              labelText: 'Importe de la bebida'),
              controller: importeController,
    ),
  );

    formWidget.add(new RaisedButton(
        color: Colors.blue,
        textColor: Colors.white,
        child: new Text('Crear Platillo'),
        onPressed: submit));
    return formWidget;
  }

 
}