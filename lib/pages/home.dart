
import 'package:flutter/material.dart';

class Home extends StatelessWidget {

  final List data = [
    {
      'nombre': "Bebidas",
      'decripcion': "CRUD para Bebidas",
      'icon':  Icon(Icons.local_bar_outlined),
      'goto' : 'bebidas'

    },
    {
      'nombre': "Clientes",
      'decripcion': "CRUD para Clientes",
      'icon':  Icon(Icons.group_outlined),
      'goto' : 'clientes'
    },
    {
      'nombre': "Mesas",
      'decripcion': "CRUD para Mesas",
      'icon':  Icon(Icons.dining_outlined),
      'goto' : 'mesas'
    },
    {
      'nombre': "Mesero",
      'decripcion': "CRUD para Mesero",
      'icon':  Icon(Icons.emoji_people_outlined),
      'goto' : 'meseros'
    },
    {
      'nombre': "Platillos",
      'decripcion': "CRUD para Platillos",
      'icon':  Icon(Icons.room_service_outlined),
      'goto' : 'platillos'
    },
    {
      'nombre': "Facturas",
      'decripcion': "CRUD para Facturas",
      'icon':  Icon(Icons.confirmation_number_outlined),
      'goto' : 'facturas'
    }
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("HOME"),
      ),
      body: ListView(
        children: 
          _listaItems(data, context),
        
      ),
    );
  }
}

  List<Widget> _listaItems(List<dynamic>? data,  context) {
   final List<Widget> opciones = [];
   data?.forEach((element) {
     final widgetTemp = ListTile(
       title: Text(element['nombre']),
       subtitle: Text(element['decripcion']),
       trailing: Icon(Icons.arrow_forward_ios),
       leading: element['icon'],
       onTap: (){
         Navigator.pushNamed(context,element['goto'], arguments: {"a"});         
         /*final route = MaterialPageRoute(
           builder: (context) => AlertPage()
        );
         Navigator.push(context, route);*/

       //  Navigator.pushNamed(context,element['ruta']);
       },
     );
     opciones.add(widgetTemp);
     opciones.add(Divider());
   });

   return opciones;
  }

