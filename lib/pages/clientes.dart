import 'package:flutter/material.dart';
import 'package:parcial03/models/Arguments.dart';
import 'package:parcial03/services/database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Clientes extends StatefulWidget {
  Clientes({Key? key}) : super(key: key);

  @override
  _ClientesState createState() => _ClientesState();
}

class _ClientesState extends State<Clientes> {
   List data = [];
  CollectionReference _collectionRef =
    FirebaseFirestore.instance.collection('clientes');
Future<void> getData() async {
    // Get docs from collection reference
    QuerySnapshot querySnapshot = await _collectionRef.get();

    // Get data from docs and convert map to List
    final allData = querySnapshot.docs.map((doc) => doc.data()).toList();

setState(() {
  this.data = allData;
});
    
    print(allData);
}
  @override
  Widget build(BuildContext context) {
    getData();
    return Scaffold(
      appBar: AppBar(title: Text("CLIENTES")),
      body: Container(
        child: Column(
          children: _listaItems(data, context),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
            Navigator.pushNamed(context,"forms", arguments: ScreenArguments(clienteForm(), "CLIENTE", "ADD"));         
        },
        child: const Icon(Icons.add_outlined),
      ),
    );
  }


    Widget clienteForm (){
  var form = Container(
    margin: EdgeInsets.all(10),
    child: Column(
      children :getFormWidget()
    )
  );

  return form;
}

  List<Widget> getFormWidget() {
    final _formKey = GlobalKey<FormState>();
  var _passKey = GlobalKey<FormFieldState>();

  final nameController = TextEditingController();
  final apellidoController = TextEditingController();
  final comentarioController = TextEditingController();
  
 void submit () {
   DatabaseService().addUser(nameController.text, apellidoController.text, comentarioController.text);
  }
    List<Widget> formWidget = [];

    formWidget.add(
      Container(
        margin: EdgeInsets.all(5),
        child: Text("NUEVO CLIENTE", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
      )
      
    );

    formWidget.add(
      new TextField(
          decoration: InputDecoration(
              hintText: 'Nombre',
              labelText: 'Nombre del Cliente'),
              controller: nameController,
    ),
  );
   
    formWidget.add(
      new TextField(
          decoration: InputDecoration(
              hintText: 'Apellido',
              labelText: 'Apellido del Cliente'),
              controller: apellidoController,
    ),
  );

  formWidget.add(
      new TextField(
          decoration: InputDecoration(
              hintText: 'Comentario',
              labelText: 'Comentario del Cliente'),
              controller: comentarioController,
    ),
  );

    formWidget.add(new RaisedButton(
        color: Colors.blue,
        textColor: Colors.white,
        child: new Text('Crear Platillo'),
        onPressed: submit));
    return formWidget;
  }


List<Widget> _listaItems(List<dynamic>? data,  context) {
   final List<Widget> opciones = [];
   data?.forEach((element) {
     final widgetTemp = ListTile(
       title: Text(element['nombre'] +" " + element['apellido']),
       trailing: Icon(Icons.arrow_forward_ios),
       leading: element['icon'],
       onTap: (){
         Navigator.pushNamed(context,element['goto'], arguments: {"a"});         
         /*final route = MaterialPageRoute(
           builder: (context) => AlertPage()
        );
         Navigator.push(context, route);*/

       //  Navigator.pushNamed(context,element['ruta']);
       },
     );
     opciones.add(widgetTemp);
     opciones.add(Divider());
   });

   return opciones;
  }
}