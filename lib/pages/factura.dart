import 'package:flutter/material.dart';

class Factura extends StatefulWidget {
  Factura({Key? key}) : super(key: key);

  @override
  _FacturaState createState() => _FacturaState();
}

class _FacturaState extends State<Factura> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("FACTURAS")),
      body: Container(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Add your onPressed code here!
        },
        child: const Icon(Icons.add_outlined),
      ),
    );
  }
}