import 'package:flutter/material.dart';

class Mesa extends StatefulWidget {
  Mesa({Key? key}) : super(key: key);

  @override
  _MesaState createState() => _MesaState();
}

class _MesaState extends State<Mesa> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("MESAS")),
      body: Container(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          // Add your onPressed code here!
        },
        child: const Icon(Icons.add_outlined),
      ),
    );
  }
}