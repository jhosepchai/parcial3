import 'package:flutter/material.dart';
import 'package:parcial03/models/Arguments.dart';
import 'package:parcial03/services/database.dart';

class Platillo extends StatefulWidget {
  Platillo({Key? key}) : super(key: key);

  @override
  _PlatilloState createState() => _PlatilloState();
}

class _PlatilloState extends State<Platillo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("PLATILLOS")),
      body: Container(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
            Navigator.pushNamed(context,"forms", arguments: ScreenArguments(platilloForm(), "PLATILLO", "ADD"));         
        },
        child: const Icon(Icons.add_outlined),
      ),
    );
  }

  Widget platilloForm (){
  var form = Container(
    margin: EdgeInsets.all(10),
    child: Column(
      children :getFormWidget()
    )
  );

  return form;
}

  List<Widget> getFormWidget() {
    final _formKey = GlobalKey<FormState>();
  var _passKey = GlobalKey<FormFieldState>();

  final nameController = TextEditingController();
  final importeController = TextEditingController();
  
 void submit () {
   DatabaseService().addPlatillo(nameController.text, importeController.text);
  }
    List<Widget> formWidget = [];

    formWidget.add(
      Container(
        margin: EdgeInsets.all(5),
        child: Text("NUEVO PLATILLO", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
      )
      
    );

    formWidget.add(
      new TextField(
          decoration: InputDecoration(
              hintText: 'Nombre',
              labelText: 'Nombre del platillo'),
              controller: nameController,
    ),
  );
   
    formWidget.add(
      new TextField(
          decoration: InputDecoration(
              hintText: 'Importe',
              labelText: 'Importe del platillo'),
              controller: importeController,
    ),
  );

    formWidget.add(new RaisedButton(
        color: Colors.blue,
        textColor: Colors.white,
        child: new Text('Crear Platillo'),
        onPressed: submit));
    return formWidget;
  }

 

  

  
}


