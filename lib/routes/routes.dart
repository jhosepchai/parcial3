
import 'package:flutter/material.dart';
import 'package:parcial03/pages/bebida.dart';
import 'package:parcial03/pages/clientes.dart';
import 'package:parcial03/pages/factura.dart';
import 'package:parcial03/pages/forms.dart';
import 'package:parcial03/pages/home.dart';
import 'package:parcial03/pages/mesa.dart';
import 'package:parcial03/pages/mesero.dart';
import 'package:parcial03/pages/platillo.dart';

Map<String, WidgetBuilder> getAplicationRoutes(){

  return <String, WidgetBuilder>{
          'home': (BuildContext context) => Home(),
          'bebidas': (BuildContext context) => Bebida(),
          'clientes': (BuildContext context) => Clientes(),
          'facturas': (BuildContext context) => Factura(),
          'mesas': (BuildContext context) => Mesa(),
          'meseros' : (BuildContext context) => Mesero(),
          'platillos' : (BuildContext context) => Platillo(),
          'forms': (BuildContext context) => Forms(),
  };
}